# cip

## Description

The purpose of this script is to quickly change my network settings as I've
found myself being a lot in the gnome network manager GUI during a work day.

## Features

- Faster configuration than the GUI.
- Choose between Static or DHCP
- Set ipv4 address
- Set submask
- Set gateway

## Dependencies

- bash
- fzf

## Known issues to solve

Routes will need to be mannualy  handled soon as there is a problem from time
to time about routes after few usage of the script. The route table seems to
populate all the entries and never delete past settings that are no longer
viable.
