#!/usr/bin/bash

# Ask the user the wanted mode (DCHP or Static)
mode=$(echo -e "1: Static\n2: DHCP"| fzf)

if [[ $mode == "1: Static" ]] || [[ $mode == "1" ]]
then # Static mode

	# Ask the user for new ip address
    ip=$(echo -e "1: 192.168.0.77\n2: Other" | fzf)
    if [[ $ip = "1: 192.168.0.77" ]]; then
        ip="192.168.0.77"
    else
        read -p "New ip: " ip
    fi

	# Ask the user for new Submask
    submasks=("255.255.255.0" "255.255.0.0" "255.255.255.252" "255.255.255.248"
                "255.255.255.240" "255.255.255.224" "255.255.255.192" 
                "255.255.255.128" "255.255.254.0" "255.255.252.0" 
                "255.255.248.0" "255.255.240.0" "255.255.224.0" "255.255.192.0"
                "255.255.255.128"
            )
    fmt_submasks=""
    for i in ${submasks[@]}; do
        fmt_submasks="$fmt_submasks$i\n"
    done
    submask=$(echo -e $fmt_submasks | fzf)

	if [[ $submask == "255.255.255.0" ]] || [[ $submask == "" ]]; then
		submask=/24
	elif [[ $submask == "255.255.0.0" ]]; then
		submask=/16
	elif [[ $submask == "255.255.255.252" ]]; then
		submask=/30
	elif [[ $submask == "255.255.255.248" ]]; then
		submask=/29
	elif [[ $submask == "255.255.255.240" ]]; then
		submask=/28
	elif [[ $submask == "255.255.255.224" ]]; then
		submask=/27
	elif [[ $submask == "255.255.255.192" ]]; then
		submask=/26
	elif [[ $submask == "255.255.255.128" ]]; then
		submask=/25
	elif [[ $submask == "255.255.254.0" ]]; then
		submask=/23
	elif [[ $submask == "255.255.252.0" ]]; then
		submask=/22
	elif [[ $submask == "255.255.248.0" ]]; then
		submask=/21
	elif [[ $submask == "255.255.240.0" ]]; then
		submask=/20
	elif [[ $submask == "255.255.224.0" ]]; then
		submask=/19
	elif [[ $submask == "255.255.192.0" ]]; then
		submask=/18
	elif [[ $submask == "255.255.128.0" ]]; then
		submask=/17
	fi

	# Ask the user for new Gateway
    gateway=$(echo -e "192.168.1.1\n192.168.1.254\nOther" | fzf)
    if [[ $gateway = "Other" ]]; then
        read -p "Please enter the new Gateway: " gateway
    fi

    nmcli connection down Wired\ connection\ 1 && sleep .1
    nmcli con modify Wired\ connection\ 1 ipv4.method manual && sleep .1
    nmcli con modify Wired\ connection\ 1 ipv4.address $ip$submask && sleep .1
    nmcli con modify Wired\ connection\ 1 ipv4.gateway $gateway && sleep .1

else # Activate DHCP
    nmcli connection down Wired\ connection\ 1 && sleep .1
    nmcli con modify Wired\ connection\ 1 ipv4.method auto && sleep .1
fi

# Apply changes
nmcli connection up Wired\ connection\ 1
